import { AppliFEWPage } from './app.po';

describe('appli-few App', () => {
  let page: AppliFEWPage;

  beforeEach(() => {
    page = new AppliFEWPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
