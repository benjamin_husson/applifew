import {Component, OnInit} from '@angular/core';

import {Repository} from '@atilf/express.js-common';

import {FEWConfig} from './few-config';
import {FEWRepositoryService} from './repository/few-repository-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  title = 'Application root component';

  constructor(private repoService: FEWRepositoryService) {
  };

  ngOnInit() {

  };

}
