import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ExpressModule } from '@atilf/express.js-angular';

import { AppComponent } from './app.component';
import { PanelComponent } from './panel.component';
import { ContentComponent } from './content.component';
import {FEWRepositoryService} from "./repository/few-repository-service";
import {RepositoryComponent} from "./repository/repository.component";

@NgModule({
  declarations: [
    AppComponent, PanelComponent, ContentComponent, RepositoryComponent
  ],
  imports: [
    ExpressModule,
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    FEWRepositoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
