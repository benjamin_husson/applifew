import { Repository } from "@atilf/express.js-common";

export class FEWConfig {
  public static readonly api: string = "https://express.atilf.fr/api";
  public static readonly repositoryId: string = "few";
  public static repository: Repository;
}
