/**
 * Created by bhusson on 26/04/17.
 */
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { FEWConfig } from '../few-config';
import { RepositoryService } from '@atilf/express.js-angular';


@Injectable()
export class FEWRepositoryService extends RepositoryService {

  constructor(http: Http) {
    super(http, FEWConfig.api);
  }

}
