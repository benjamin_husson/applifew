import { TestBed, async } from '@angular/core/testing';

import {RepositoryComponent} from './repository.component';

describe('RepositoryComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RepositoryComponent
      ],
    }).compileComponents();
  }));

  it('should create the repository', async(() => {
    const fixture = TestBed.createComponent(RepositoryComponent);
    const repository = fixture.debugElement.componentInstance;
    expect(repository).toBeTruthy();
  }));

  it(`should have 'few' as repository.name`, async(() => {
    const fixture = TestBed.createComponent(RepositoryComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.repository.name).toEqual('few');
  }));

});
