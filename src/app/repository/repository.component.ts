import {Component, OnInit} from '@angular/core';

import {Repository} from '@atilf/express.js-common';

import {FEWConfig} from '../few-config';
import {FEWRepositoryService} from './few-repository-service';

@Component({
  selector: 'panel-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.scss']
})

export class RepositoryComponent implements OnInit {

  repository: Repository;

  constructor(private repoService: FEWRepositoryService) {
  };

  ngOnInit() {
    this.repoService.getRepository(FEWConfig.repositoryId).subscribe(
      (repository) => {
        this.repository = repository;
      }
    );
  };

}
